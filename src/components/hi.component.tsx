const Hi = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="254"
      height="263"
      fill="none"
      viewBox="0 0 254 263"
    >
      <g className="hi-svg">
        <g className="h1">
          <mask
            id="a"
            width={55}
            height={220}
            x={19}
            y={33}
            className="a"
            maskUnits="userSpaceOnUse"
            style={{
              maskType: 'alpha',
            }}
          >
            <path
              fill="#fff"
              d="M28.5 55.5c0 6.5-3.5 74.5-5 92.5-1 10.833-3.2 36.8-4 54-1 21.5.5 33.5 15.5 44s28 6 29 3.5-1-16.5-1-24.5 8.5-90.5 10.5-139.5S44 32 36 33.5s-7.5 15.5-7.5 22Z"
              className="Vector 6"
            />
          </mask>
          <g mask="url(#a)">
            <path
              fill="red"
              d="M15.5 203c-1 21.5 5.5 40.5 20.5 51C96.58 296.407 80.95 87.206 82 61.5 83.326 29.012 19.5-15.47 19.5 33c0 56.845-1.36 113.229-4 170Z"
              className="sprite"
            />
          </g>
        </g>
        <g className="h2">
          <mask
            id="b"
            width={57}
            height={221}
            x={100}
            y={33}
            className="b"
            maskUnits="userSpaceOnUse"
            style={{
              maskType: 'alpha',
            }}
          >
            <path
              fill="#fff"
              d="M110.244 55.5c0 6.5-3.5 74.501-5 92.501-1 10.833-3.2 36.8-4 54-1 21.5-.744 33.999 14.256 44.499s29.1 7.5 30.5 4c1.256-3.14-.744-10.501 0-26.5.372-7.991 8-96 10-145 1.735-42.5-26.5-47.703-38.256-45.5-8 1.5-7.5 15.5-7.5 22Z"
              className="Vector 7"
            />
          </mask>
          <g mask="url(#b)">
            <path
              fill="red"
              d="M115.5 15c-8 1.5-14.256 23.5-14.256 30 0 49.534 2.304 97.472 0 147.001-1 21.5-.744 33.999 14.256 44.499 62.353 43.647 42.939-145.51 44-171.5 1.735-42.5-32.244-52.204-44-50Z"
              className="sprite"
            />
          </g>
        </g>
        <g className="i">
          <mask
            id="c"
            width={54}
            height={169}
            x={185}
            y={85}
            className="c"
            maskUnits="userSpaceOnUse"
            style={{
              maskType: 'alpha',
            }}
          >
            <path
              fill="#fff"
              d="M199 86c-5 .937-4.165 8.524-5 17.499-3.999 43.001-5.311 65.198-6.999 101.501-1 21.5-5.473 32.032 13.499 42.5 14.5 8 25 6.5 26 2 .734-3.301 2.598-44.501 5.5-71.501 3.117-29 5-45.999 6.5-65 1.379-17.46-27.744-29.204-39.5-27Z"
              className="Vector 8"
            />
          </mask>
          <g mask="url(#c)">
            <path
              fill="red"
              d="M200.5 68c-8 1.5-14.256 23.5-14.256 30 0 49.534-2.196 67.972-4.5 117.501-1 21.5-.744 33.999 14.256 44.499 62.353 43.647 47.439-116.011 48.5-142 1.735-42.5-32.244-52.204-44-50Z"
              className="sprite"
            />
          </g>
        </g>
        <g className="idot">
          <mask
            id="d"
            width={51}
            height={50}
            x={191}
            y={6}
            className="d"
            maskUnits="userSpaceOnUse"
            style={{
              maskType: 'alpha',
            }}
          >
            <path
              fill="#fff"
              d="M205 52.5c8.5 4 27 3.5 30 2 1.5-1.5 5.5-32 6.5-38.5 1.462-9.5-21.107-9.197-28-9-17.5.5-20 1.5-22 18.5s3.786 22.429 13.5 27Z"
              className="Vector 9"
            />
          </mask>
          <g mask="url(#d)">
            <path
              fill="red"
              d="M202.346 58.111c10.612 5.014 33.709 4.388 37.454 2.508 1.873-1.88 6.867-40.111 8.115-48.26C249.74.453 221.564.832 212.958 1.08c-21.848.626-24.969 1.88-27.466 23.189s4.727 28.114 16.854 33.844Z"
              className="sprite"
            />
          </g>
        </g>
        <g className="hh">
          <mask
            id="e"
            width={170}
            height={46}
            x={4}
            y={123}
            className="e"
            maskUnits="userSpaceOnUse"
            style={{
              maskType: 'alpha',
            }}
          >
            <path
              fill="#fff"
              d="M8 137.5c8-3.5 54-8 76-9.5 6-.666 22.2-2.4 39-4 21-2 29.5 5 36 8.5s15 12 14 19-13.476 5.319-18 5.5c-12.5.5-57.516 6.035-64.5 6.5-15 1-48 5.5-58.5 4.5s-21.5-12-25-16.5-3.955-11.832 1-14Z"
              className="Vector 10"
            />
          </mask>
          <g mask="url(#e)">
            <path
              fill="red"
              d="M2.5 153.5C-1 149 2.045 123.167 7 121c15.578-6.816 145.017-5.068 160 3 6.5 3.5 10.5 28.5 9.5 35.5-1.696 11.869-134.076 15.017-150 13.5-10.5-1-20.5-15-24-19.5Z"
              className="sprite"
            />
          </g>
        </g>
      </g>
    </svg>
  )
}

export default Hi
